package org.amrun.mockserver.persistence.domain;

import org.amrun.mockserver.api.RelatedRequest;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Document(collection = "Request")
public class Request {

    @Id
    private String id;

    @Field("Identifier")
    private String identifier;

    @Field("Name")
    private String name;

    @Field("Domain_Name")
    private String domainName;

    @Field("Path")
    private String path;

    @Field("FullResourcePath")
    private String fullResourcePath;

    @Field("QueryParams")
    private List<QueryParameter> queryParams;

    @Field("Http_Method")
    private String httpMethod;

    @Field("RequestBody")
    private String requestBody;

    @Field("RequestContentType")
    private String requestContentType;

    @Field("Response_Code")
    private int responseCode;

    @Field("Content_Type")
    private String contentType;

    @Field("Response_Body")
    private String responseBody;

    @Transient
    private List<RelatedRequest> relatedIdentifiers = new ArrayList<>();

    @Transient
    private String fullUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFullResourcePath() {
        return fullResourcePath;
    }

    public void setFullResourcePath(String fullResourcePath) {
        this.fullResourcePath = fullResourcePath;
    }

    public List<QueryParameter> getQueryParams() {
        return queryParams;
    }

    public void setQueryParams(List<QueryParameter> queryParams) {
        this.queryParams = queryParams;
    }


    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public List<RelatedRequest> getRelatedIdentifiers() {
        return relatedIdentifiers;
    }

    public void setRelatedIdentifiers(List<RelatedRequest> relatedIdentifiers) {
        this.relatedIdentifiers = relatedIdentifiers;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getRequestContentType() {
        return requestContentType;
    }

    public void setRequestContentType(String requestContentType) {
        this.requestContentType = requestContentType;
    }

    @Override
    public String toString() {
        return "Request{" +
                "id='" + id + '\'' +
                ", identifier='" + identifier + '\'' +
                ", name='" + name + '\'' +
                ", domainName='" + domainName + '\'' +
                ", path='" + path + '\'' +
                ", fullResourcePath='" + fullResourcePath + '\'' +
                ", queryParams=" + queryParams +
                ", httpMethod='" + httpMethod + '\'' +
                ", requestBody='" + requestBody + '\'' +
                ", requestContentType='" + requestContentType + '\'' +
                ", responseCode=" + responseCode +
                ", contentType='" + contentType + '\'' +
                ", responseBody='" + responseBody + '\'' +
                ", relatedIdentifiers=" + relatedIdentifiers +
                ", fullUrl='" + fullUrl + '\'' +
                '}';
    }
}
