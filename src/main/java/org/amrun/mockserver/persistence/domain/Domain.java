package org.amrun.mockserver.persistence.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.util.List;

@Document(collection = "Domain")
public class Domain {

    @Id
    private String id;

    @Field(value = "Name")
    @NotNull
    private String name;

    @Field("Alias")
    @NotNull
    private List<String> alias;

    @Field("ResourcePath")
    @NotNull
    private String resourcePath;

    /**
     * Which server should be called in case a mock is not found.
     */
    @Field("ProxyUrl")
    private String proxyUrl;

    @Field("QueryParams")
    private List<QueryParameter> commonQueryParameter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAlias() {
        return alias;
    }

    public void setAlias(List<String> alias) {
        this.alias = alias;
    }

    public String getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public String getProxyUrl() {
        return proxyUrl;
    }

    public void setProxyUrl(String proxyUrl) {
        this.proxyUrl = proxyUrl;
    }

    public List<QueryParameter> getCommonQueryParameter() {
        return commonQueryParameter;
    }

    public void setCommonQueryParameter(List<QueryParameter> commonQueryParameter) {
        this.commonQueryParameter = commonQueryParameter;
    }
}
