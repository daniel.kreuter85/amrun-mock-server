package org.amrun.mockserver.persistence.repository;

import org.amrun.mockserver.persistence.domain.Request;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface RequestRepository extends MongoRepository<Request, String> {

    List<Request> findAllByPath(String path);

    Optional<Request> findOneByIdentifier(String identifier);

    List<Request> findAllByFullResourcePath(String path);
}
