package org.amrun.mockserver.persistence.repository;

import org.amrun.mockserver.persistence.domain.Domain;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface DomainRepository extends MongoRepository<Domain, String> {

    Optional<Domain> findOneByName(String name);
}
