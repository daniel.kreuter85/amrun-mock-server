package org.amrun.mockserver.api;

public class RelatedRequest {

    private Boolean relatedRequestExists;

    private String identifier;

    private String domainName;

    private String requestName;

    private String value;

    private String display;

    public Boolean getRelatedRequestExists() {
        return relatedRequestExists;
    }

    public void setRelatedRequestExists(Boolean relatedRequestExists) {
        this.relatedRequestExists = relatedRequestExists;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }
}
