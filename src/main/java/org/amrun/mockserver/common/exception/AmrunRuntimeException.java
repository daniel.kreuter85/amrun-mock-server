package org.amrun.mockserver.common.exception;

/**
 * Exception class that is used for internal exceptions inside the controllers.
 */
public class AmrunRuntimeException extends RuntimeException {

    public AmrunRuntimeException() {
    }

    public AmrunRuntimeException(String message) {
        super(message);
    }

    public AmrunRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public AmrunRuntimeException(Throwable cause) {
        super(cause);
    }

    public AmrunRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
