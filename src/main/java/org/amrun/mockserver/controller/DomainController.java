package org.amrun.mockserver.controller;

import org.amrun.mockserver.persistence.domain.Domain;
import org.amrun.mockserver.persistence.domain.Request;
import org.amrun.mockserver.persistence.repository.DomainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class DomainController {

    @Autowired
    private DomainRepository repository;

    @GetMapping("/api/domains")
    public List<Domain> getDomains() {
        return repository.findAll();
    }

    @PostMapping("/api/domains")
    public Domain create(@Valid @RequestBody Domain domain) {
        return repository.save(domain);
    }

    @GetMapping("/api/domains/{id}")
    public Domain getDomain(@PathVariable("id") String id) {
        return repository.findById(id).orElse(null);
    }

    @PutMapping(value = "/api/domains/{id}")
    public Domain update(@PathVariable("id") String id, @Valid @RequestBody Domain domain) {
        domain.setId(id);
        return repository.save(domain);
    }


    @DeleteMapping(value = "/api/domains/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.deleteById(id);
    }

}
