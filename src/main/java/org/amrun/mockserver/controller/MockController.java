package org.amrun.mockserver.controller;

import io.micrometer.core.instrument.util.IOUtils;
import org.amrun.mockserver.helper.QueryParamsHelper;
import org.amrun.mockserver.persistence.domain.Domain;
import org.amrun.mockserver.persistence.domain.QueryParameter;
import org.amrun.mockserver.persistence.domain.Request;
import org.amrun.mockserver.persistence.repository.DomainRepository;
import org.amrun.mockserver.persistence.repository.RequestRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class MockController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockController.class);

    @Autowired
    private RequestRepository repository;

    @Autowired
    private DomainRepository domainRepository;

    private RestTemplate restTemplate;

    private List<Domain> domains;

    @PostConstruct
    public void init() {
        domains = domainRepository.findAll();
        restTemplate = new RestTemplate();
    }

    @GetMapping(value = "/mocks/api/**")
    public ResponseEntity<Object> mock(HttpServletRequest request, @RequestParam Map<String, String> params, @RequestHeader MultiValueMap<String, String> headers) {
        ResponseEntity<Object> response = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        String requestURI = request.getRequestURI();
        requestURI = StringUtils.replace(requestURI, "/mocks/api", "");
        LOGGER.info("Incoming Request for URI {} and query parameters {}", requestURI, params);
        LOGGER.info("Client provided the following headers: {}", headers);

        List<Request> allByPath = repository.findAllByFullResourcePath(requestURI);
        boolean mockFound = false;
        for (int i = 0; i < allByPath.size() && !mockFound; i++) {
            Request req = allByPath.get(i);

            Map<String, String> paramMap = new LinkedHashMap<>();
            // grt domain, inside of the domain we have the common query parameter that exist for every request
            Optional<Domain> domain = domainRepository.findOneByName(req.getDomainName());
            if (domain.isPresent()) {
                Domain dom = domain.get();
                if(!CollectionUtils.isEmpty(dom.getCommonQueryParameter())) {
                    for(QueryParameter parameter : dom.getCommonQueryParameter()) {
                        paramMap.put(parameter.getKey(), parameter.getValue());
                    }
                }
                paramMap = QueryParamsHelper.mergeQueryParameters(dom.getCommonQueryParameter(), req.getQueryParams());
            }
            if (params != null && params.equals(paramMap)) {
                response = returnResponse(req);
                mockFound = true;
            }
        }

        if (!mockFound) {
            // route request to target system.
            Optional<Domain> targetDomain = Optional.empty();
            for (int i = 0; i < domains.size() && !targetDomain.isPresent(); i++) {
                Domain domain = domains.get(i);
                targetDomain = requestURI.contains(domain.getResourcePath()) ? Optional.of(domain) : Optional.empty();
            }
            // if targetDomain is found, forward the call
            if(targetDomain.isPresent()) {
                Domain target = targetDomain.get();
                if(target.getProxyUrl() != null) {
                    response = forwardRequest(target, request, requestURI, headers);
                }

            }
        }


        return response;
    }

    @PostMapping(value = "/mocks/api/**")
    public ResponseEntity<Object> mockPost(HttpServletRequest request, @RequestParam Map<String,String> params, @RequestHeader MultiValueMap<String, String> headers, @RequestBody(required = false) String requestBody) {
        return handlePostPutRequest(request, params, headers, requestBody, "POST");
    }

    @PutMapping(value = "/mocks/api/**")
    public ResponseEntity<Object> mockPut(HttpServletRequest request, @RequestParam Map<String,String> params, @RequestHeader MultiValueMap<String, String> headers, @RequestBody(required = false) String requestBody) {
        return handlePostPutRequest(request, params, headers, requestBody, "PUT");
    }

    private ResponseEntity<Object> handlePostPutRequest(HttpServletRequest request, Map<String, String> params, MultiValueMap<String, String> headers, String requestBody, String method) {
        ResponseEntity<Object> response = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        String requestURI = request.getRequestURI();
        requestURI = StringUtils.replace(requestURI, "/mocks/api", "");
        LOGGER.info("Incoming {} Request {} with params {} and body {}, body's type = {}", method,
                requestURI,
                params,
                requestBody,
                requestBody != null ? requestBody.getClass(): "");
        List<Request> allByFullResourcePath = repository.findAllByFullResourcePath(requestURI);
        boolean mockFound = false;
        LOGGER.info("Did we find a request? Count: {}. URI: {}", allByFullResourcePath.size(), requestURI);
        for (int i = 0; i < allByFullResourcePath.size() && !mockFound; i++) {
            Request req = allByFullResourcePath.get(i);
            if(!req.getHttpMethod().equals(method)) {
                continue;
            }
            boolean queryParamsMatch = false;
            boolean containsQueryParams = false;
            if (params != null && params.size() > 0) {
                containsQueryParams = true;
                queryParamsMatch = params.equals(QueryParamsHelper.mergeQueryParameters(null, req.getQueryParams()));
            }
            String body = "";
            if(!StringUtils.isEmpty(req.getRequestBody()) && !StringUtils.isEmpty(requestBody)) {
                body = req.getRequestBody();
                body = body.replaceAll("\\s", "");
                body = body.replaceAll("\n", "");
                requestBody = requestBody.replaceAll("\\s", "");
                requestBody = requestBody.replaceAll("\n", "");
            }
            boolean requestBodyMatches = StringUtils.isEmpty(body) || body.equals(requestBody); // when request body is null, we say it's true, in that case we expect query params to exist.
            LOGGER.info("Are the request bodies equal to each other? Answer: {}", requestBodyMatches);
            if (containsQueryParams) {
                if (queryParamsMatch && requestBodyMatches) {
                    response = returnResponse(req);
                    mockFound = true;
                }
            } else {
                if(requestBodyMatches) {
                    response = returnResponse(req);
                    mockFound = true;
                }
            }
        }

        if (!mockFound) {
            // route request to proxy system.
            Optional<Domain> targetDomain = Optional.empty();
            for (int i = 0; i < domains.size() && !targetDomain.isPresent(); i++) {
                Domain domain = domains.get(i);
                targetDomain = requestURI.contains(domain.getResourcePath()) ? Optional.of(domain) : Optional.empty();
            }
            // if targetDomain is found, forward the call
            if(targetDomain.isPresent()) {
                Domain target = targetDomain.get();
                response = forwardRequest(target, request, requestURI, headers);

            }
        }

        return response;
    }

    private ResponseEntity<Object> returnResponse(Request req) {
        ResponseEntity<Object> response;
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", req.getContentType());
        responseHeaders.add("X-Powered-By", "AmrunMockServer");
        LOGGER.info("Before response");
        response = new ResponseEntity<>(req.getResponseBody(), responseHeaders, HttpStatus.resolve(req.getResponseCode()));
        LOGGER.info("After response");
        return response;
    }

    private ResponseEntity<Object> forwardRequest(Domain target, HttpServletRequest request, String targetUri, MultiValueMap<String, String> headers) {
        ResponseEntity<Object> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        if(StringUtils.isEmpty(target.getProxyUrl())) {
            return response;
        }
        try {
            String body = IOUtils.toString(request.getInputStream(), Charset.forName(request.getCharacterEncoding()));
            HttpEntity<Object> entity = new HttpEntity<>(body, headers);
            String targetUrl = targetUri.replace(target.getResourcePath(), "");
            targetUrl = target.getProxyUrl() + targetUrl;
            response = restTemplate.exchange(targetUrl,
                    HttpMethod.valueOf(request.getMethod()),
                    entity,
                    Object.class,
                    request.getParameterMap());

        } catch (IOException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }
        return response;
    }
}
