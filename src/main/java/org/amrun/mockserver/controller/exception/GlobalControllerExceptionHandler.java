package org.amrun.mockserver.controller.exception;

import org.amrun.mockserver.common.exception.AmrunRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception handler to map internal exceptions to an appropriate http status code.
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(AmrunRuntimeException.class)
    public void handleAmrunRuntimeException() {
        // nothing to do.
    }
}
