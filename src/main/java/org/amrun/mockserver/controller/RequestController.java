package org.amrun.mockserver.controller;

import com.google.common.base.Joiner;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.amrun.mockserver.api.RelatedRequest;
import org.amrun.mockserver.common.exception.AmrunRuntimeException;
import org.amrun.mockserver.helper.QueryParamsHelper;
import org.amrun.mockserver.persistence.domain.Domain;
import org.amrun.mockserver.persistence.domain.QueryParameter;
import org.amrun.mockserver.persistence.domain.Request;
import org.amrun.mockserver.persistence.repository.DomainRepository;
import org.amrun.mockserver.persistence.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.groupingBy;

@RestController
public class RequestController {

    @Autowired
    private RequestRepository repository;

    @Autowired
    private DomainRepository domainRepository;

    private List<Domain> domains = null;

    @PostConstruct
    public void init() {
        this.domains = domainRepository.findAll();
    }

    @RequestMapping(value = "/api/requests", method = RequestMethod.GET)
    public Map<String, List<Request>> getAllRequests() {
        return repository.findAll()
                .stream()
                .map(this::computeFullUrl)
                .map(this::computeRelatedRequests)
                .collect(groupingBy(Request::getDomainName));
    }


    @RequestMapping("/api/requests/{id}")
    public Request getRequest(@PathVariable("id") String id) {
        Optional<Request> request = repository.findById(id);
        request.ifPresent(req -> {
            req.setFullUrl(computeFullUrl(req).getFullUrl());
            req.setRelatedIdentifiers(computeRelatedRequests(req).getRelatedIdentifiers());
        });
        return request.orElse(null);
    }

    @PutMapping(value = "/api/requests/{id}")
    public Request update(@PathVariable("id") String id, @Valid @RequestBody Request request) {
        request.setId(id);
        request.setFullResourcePath(getFullResourcePath(request));
        return repository.save(request);
    }

    @PostMapping(value = "/api/requests")
    public Request create(@Valid @RequestBody Request request) {
        this.domains = domainRepository.findAll();
        if(!StringUtils.isEmpty(request.getId())) {
            request.setId(null);    // with this we create a new request. We do this to be able to copy an existing one.
        }
        request.setFullResourcePath(getFullResourcePath(request));
        return repository.save(request);
    }



    @DeleteMapping(value = "/api/requests/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.deleteById(id);
    }

    private String getFullResourcePath(Request request) {
        String fullResourcePath = request.getPath();
        Optional<Domain> domainName = domains.stream()
                .filter(domain -> domain.getName().equals(request.getDomainName()))
                .findAny();
        Domain domain = domainName.orElseThrow(() -> new AmrunRuntimeException("You can not create a request for a nonexistent domain."));
        if (!fullResourcePath.startsWith("/")) {
            fullResourcePath = "/" + fullResourcePath;
        }
        fullResourcePath = domain.getResourcePath() + fullResourcePath;
        if(fullResourcePath.contains("{id}") && !StringUtils.isEmpty(request.getIdentifier())) {
            fullResourcePath = fullResourcePath.replace("{id}", request.getIdentifier());
        } else {
            if(!StringUtils.isEmpty(request.getIdentifier())) {
                if (!fullResourcePath.endsWith("/")) {
                    fullResourcePath += "/";
                }
                fullResourcePath += request.getIdentifier();
            }
        }
        return fullResourcePath;
    }


    private Request computeRelatedRequests(Request request) {
        String jsonPathRootPrefix = "$.";
        String jsonPathChildrenPrefix = "$.[*].";
        List<RelatedRequest> relatedRequests = new ArrayList<>();

        try {
            DocumentContext jsonContext = JsonPath.parse(request.getResponseBody());

            for(Domain domain : domains) {
                for(String identifierAlias : domain.getAlias()) {
                    String identifier = "" + jsonContext.read(jsonPathRootPrefix + identifierAlias);
                    if(StringUtils.isEmpty(identifier)) {
                        // it is possible, that related identifiers are somewhere deeper inside the json object so we check for them now.
                        identifier = jsonContext.read(jsonPathChildrenPrefix + identifierAlias);
                    }
                    if (!StringUtils.isEmpty(identifier)) {
                        final String id = identifier;   // used as a trick, otherwise we can not set the identifier for the related
                        // request inside of a lambda expression. Identifier can not be final
                        // because it's value is set at most twice which is against final.
                        // once we reach this line, we can create a new variable because the
                        // value of identifier won't change anymore.
                        Optional<Request> relatedRequest = repository.findOneByIdentifier(identifier);
                        relatedRequest.ifPresent(relRequest -> {
                            RelatedRequest req = new RelatedRequest();
                            req.setDomainName(domain.getName());
                            req.setIdentifier(relRequest.getIdentifier());
                            req.setRelatedRequestExists(true);
                            req.setRequestName(relRequest.getName());
                            req.setValue(relRequest.getIdentifier());
                            req.setDisplay(domain.getName() + ": ID: " + relRequest.getIdentifier());
                            relatedRequests.add(req);
                        });
                        relatedRequest.orElseGet(() -> {

                            RelatedRequest req = new RelatedRequest();
                            req.setDomainName(domain.getName());
                            req.setRelatedRequestExists(false);
                            req.setIdentifier(id);
                            relatedRequests.add(req);
                            req.setDisplay(domain.getName() + ": ID: " + id + " not mocked.");
                            return null;
                        });
                    }
                }
            }

        } catch (Exception e) {
            // maybe we don't have json?
            // it might also occur that there is no result for an identifier inside of jsonpath, so we can ignore the exceptions here as well.
        }
        request.setRelatedIdentifiers(relatedRequests);
        return request;
    }

    private Request computeFullUrl(Request request) {
        StringBuilder fullUrl = new StringBuilder(request.getFullResourcePath());
        Domain domain = domainRepository.findOneByName(request.getDomainName()).orElseThrow(() -> new IllegalArgumentException("You can not create a request without a domain"));

        List<QueryParameter> queryParams = request.getQueryParams();
        Map<String, String> queryParameters = QueryParamsHelper.mergeQueryParameters(domain.getCommonQueryParameter(), queryParams);
        if(queryParameters.size() > 0) {
            fullUrl.append("?").append(Joiner.on("&").withKeyValueSeparator("=").join(queryParameters));
        }
        request.setFullUrl(fullUrl.toString());
        return request;
    }

}
