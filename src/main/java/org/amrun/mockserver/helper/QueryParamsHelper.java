package org.amrun.mockserver.helper;

import org.amrun.mockserver.persistence.domain.QueryParameter;
import org.springframework.util.CollectionUtils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class QueryParamsHelper {

    public static Map<String, String> mergeQueryParameters(List<QueryParameter> commonQueryParams, List<QueryParameter> requestQueryParams) {
        Map<String, String> result = new LinkedHashMap<>();
        if(!CollectionUtils.isEmpty(commonQueryParams)) {
            for(QueryParameter parameter : commonQueryParams) {
                result.put(parameter.getKey(), parameter.getValue());
            }
        }
        if(!CollectionUtils.isEmpty(requestQueryParams)) {
            for (QueryParameter parameter : requestQueryParams) {
                if(result.get(parameter.getKey()) != null) {
                    result.replace(parameter.getKey(), parameter.getValue());
                } else {
                    result.put(parameter.getKey(), parameter.getValue());
                }
            }
        }
        return result;
    }
}
