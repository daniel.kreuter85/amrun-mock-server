package org.amrun.mockserver.configuration;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.GenericContainer;

public class MongoDBInitializer {

    public static void  initialize(ConfigurableApplicationContext configurableApplicationContext, GenericContainer mongodb) {

        TestPropertyValues values = TestPropertyValues.of("spring.data.mongodb.host=" + mongodb.getContainerIpAddress(),
                "spring.data.mongodb.port=" + mongodb.getFirstMappedPort(), "spring.data.mongodb.database=MocksDbIT");
        values.applyTo(configurableApplicationContext);
    }
}
