package org.amrun.mockserver.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.amrun.mockserver.configuration.MongoDBInitializer;
import org.amrun.mockserver.helper.JsonHelper;
import org.amrun.mockserver.persistence.domain.Domain;
import org.amrun.mockserver.persistence.domain.Request;
import org.amrun.mockserver.persistence.repository.DomainRepository;
import org.amrun.mockserver.persistence.repository.RequestRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.GenericContainer;

import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = RequestControllerIT.Initializer.class)
public class RequestControllerIT {

    private static final int MONGODB_PORT = 27017;

    private static final String TEST_RESOURCES_PATH = "src/test/resources/mock_data/";

    // Base path of the mvc controller we want to test.
    private static final String BASE_PATH = "/api/requests";

    @ClassRule
    public static GenericContainer mongodb = new GenericContainer<>("mongo:latest").withExposedPorts(MONGODB_PORT);

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Autowired
    private DomainRepository domainRepository;

    @Autowired
    private RequestRepository requestRepository;

    private Request request;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void initTestDatabase() throws Exception {
        ObjectMapper jsonMapper = new ObjectMapper();
        List<Domain> domains = jsonMapper.readValue(Paths.get(TEST_RESOURCES_PATH + "domains.json").toFile(), new TypeReference<List<Domain>>() {});
        request = jsonMapper.readValue(Paths.get(TEST_RESOURCES_PATH + "request_4711.json").toFile(), Request.class);
        domains.forEach(domain -> {
            domainRepository.save(domain);
        });
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @After
    public void cleanup() {
        requestRepository.deleteAll();
        domainRepository.deleteAll();
    }

    @Test
    public void creating_a_request_will_be_successful() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(BASE_PATH)
                .content(JsonHelper.asJsonString(request))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$.fullResourcePath").value("/spring-petclinic/api/pettypes/4711/name"));
    }

    @Test
    public void get_request_from_controller_has_full_url_set() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(BASE_PATH)
                    .content(JsonHelper.asJsonString(request))
                    .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
        Request persistedRequest = requestRepository.findOneByIdentifier("4711").orElseThrow(() -> new IllegalStateException("Request was not created, can not continue tests."));
        this.mockMvc.perform(
                MockMvcRequestBuilders.get(BASE_PATH + "/" + persistedRequest.getId())
        ).andExpect(status().isOk());

    }

    @Test
    public void update_request_results_in_updated_full_resource_path() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonHelper.asJsonString(request))
        ).andExpect(status().isOk());
        Request persistedRequest = requestRepository.findOneByIdentifier("4711").orElseThrow(() -> new IllegalStateException("Request was not created, can not continue tests."));
        persistedRequest.setIdentifier("4712");
        mockMvc.perform(
                MockMvcRequestBuilders.put(BASE_PATH + "/" + persistedRequest.getId())
                .content(JsonHelper.asJsonString(persistedRequest))
                .contentType(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk())
                .andExpect(jsonPath("$.fullResourcePath").value("/spring-petclinic/api/pettypes/4712/name"));
    }

    @Test
    public void deleting_request_deletes_request() throws Exception {
        Request persistedRequest = requestRepository.save(request);
        mockMvc.perform(
                MockMvcRequestBuilders.delete(BASE_PATH + "/" + persistedRequest.getId())
        ).andExpect(status().isOk());
        persistedRequest = requestRepository.findOneByIdentifier("4711").orElse(null);
        assertThat(persistedRequest, is(nullValue()));
    }

    @Test
    public void create_request_without_domain_name_results_in_bad_request() throws Exception {
        //try {
            request.setDomainName(null);
            mockMvc.perform(
                    MockMvcRequestBuilders.post(BASE_PATH)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(JsonHelper.asJsonString(request))
            ).andExpect(status().isBadRequest());
        /*} catch (Exception e) {
            // we get an exception from the controller, we can ignore it here since we get the expected rest result.
        }*/
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            MongoDBInitializer.initialize(configurableApplicationContext, mongodb);
        }
    }
}
