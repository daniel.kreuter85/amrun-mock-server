package org.amrun.mockserver.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.amrun.mockserver.configuration.MongoDBInitializer;
import org.amrun.mockserver.persistence.domain.Domain;
import org.amrun.mockserver.persistence.domain.Request;
import org.amrun.mockserver.persistence.repository.DomainRepository;
import org.amrun.mockserver.persistence.repository.RequestRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.GenericContainer;

import java.nio.file.Paths;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = MocksControllerIT.Initializer.class)
public class MocksControllerIT {

    private static final int MONGODB_PORT = 27017;

    private static final String BASE_PATH = "/mocks/api";

    private static final String TEST_RESOURCES_PATH = "src/test/resources/mock_data/";

    @ClassRule
    public static GenericContainer mongodb = new GenericContainer<>("mongo:latest").withExposedPorts(MONGODB_PORT);

    @Autowired
    private RequestRepository requestRepository;

    @Autowired
    private DomainRepository domainRepository;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void init() throws Exception {
        ObjectMapper jsonMapper = new ObjectMapper();
        List<Domain> domains = jsonMapper.readValue(Paths.get(TEST_RESOURCES_PATH + "domains.json").toFile(), new TypeReference<List<Domain>>() {});
        List<Request> requests = jsonMapper.readValue(Paths.get(TEST_RESOURCES_PATH + "requests.json").toFile(), new TypeReference<List<Request>>() {});
        domains.forEach(domain -> domainRepository.save(domain));
        requests.forEach(request -> requestRepository.save(request));
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @After
    public void cleanup() {
        requestRepository.deleteAll();
        domainRepository.deleteAll();
    }

    @Test
    public void calling_get_mock_results_mock_response() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get(BASE_PATH + "/spring-petclinic/api/pettypes")
        ).andExpect(status().isOk());
    }

    @Test
    public void calling_post_mock_results_in_mock_response() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post(BASE_PATH + "/spring-petclinic/api/pettypes")
                .content("{\n    \"username\": \"secondAdmin\",\n    \"password\": \"password\",\n    \"enabled\": true,\n    \"roles\": [\n    \t{ \"name\" : \"OWNER_ADMIN\" }\n\t]\n}")
                .contentType("application/json")
        ).andExpect(status().isOk());
    }

    @Test
    public void calling_post_mock_with_query_params_results_in_mock_response() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post(BASE_PATH + "/spring-petclinic/api/pettypes?executor=FISH")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n  \"id\": 42,\n  \"name\": \"FISH\"\n}")
        ).andExpect(status().isOk());
    }

    @Test
    public void post_mock_without_body_returns_response() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post(BASE_PATH + "/spring-petclinic/api/pettypes?executor=FISH")
        ).andExpect(status().isOk());
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            MongoDBInitializer.initialize(configurableApplicationContext, mongodb);
        }
    }
}
