package org.amrun.mockserver.controller;

import org.amrun.mockserver.configuration.MongoDBInitializer;
import org.amrun.mockserver.helper.JsonHelper;
import org.amrun.mockserver.persistence.domain.Domain;
import org.amrun.mockserver.persistence.repository.DomainRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testcontainers.containers.GenericContainer;

import java.util.ArrayList;
import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = DomainControllerIT.Initializer.class)
public class DomainControllerIT {

    private static final int MONGODB_PORT = 27017;

    private static final String API_LINK = "/api/domains";

    private MockMvc mockMvc;

    @ClassRule
    public static GenericContainer mongodb = new GenericContainer<>("mongo:latest").withExposedPorts(MONGODB_PORT);

    @Autowired
    private DomainRepository domainRepository;

    @LocalServerPort
    private int serverPort;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void init() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @After
    public void cleanup() {
        domainRepository.deleteAll();
    }

    @Test
    public void creating_domain_successful() throws Exception {
        Domain domain = new Domain();
        domain.setName("TestDomain");
        domain.setResourcePath("/test");
        domain.setAlias(new ArrayList<>());
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(API_LINK)
                        .content(JsonHelper.asJsonString(domain))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
            )
                .andExpect(status().isOk());
    }

    @Test
    public void creating_domain_without_alias_results_in_bad_request() throws Exception {
        Domain domain = new Domain();
        domain.setName("TestDomain");
        domain.setResourcePath("/test");
        this.mockMvc.perform(
                MockMvcRequestBuilders.post(API_LINK)
                .content(JsonHelper.asJsonString(domain))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
            ).andExpect(status().isBadRequest());
    }

    @Test
    public void updating_domain_alias_results_in_updated_domain() throws Exception {
        Domain domain = new Domain();
        domain.setName("TestDomain");
        domain.setResourcePath("/test");
        domain.setAlias(new ArrayList<>());
        Domain persistedDomain = domainRepository.save(domain);
        persistedDomain.setAlias(Collections.singletonList("test-id"));
        this.mockMvc.perform(
                MockMvcRequestBuilders.put(API_LINK + "/" + persistedDomain.getId())
                    .content(JsonHelper.asJsonString(domain))
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
        ).andExpect(status().isOk());
    }

    @Test
    public void deleting_domain_results_in_empty_database() throws Exception {
        Domain domain = new Domain();
        domain.setName("TestDomain");
        domain.setResourcePath("/test");
        domain.setAlias(new ArrayList<>());
        Domain persistedDomain = domainRepository.save(domain);
        this.mockMvc.perform(
                MockMvcRequestBuilders.delete(API_LINK + "/" + persistedDomain.getId())
        ).andExpect(status().isOk());
        persistedDomain = domainRepository.findOneByName("TestDomain").orElse(null);
        assertThat(persistedDomain, is(nullValue()));
    }

    @Test
    public void getting_domain_from_controller_returns_expected_result() throws Exception {
        Domain domain = new Domain();
        domain.setName("TestDomain");
        domain.setResourcePath("/test");
        domain.setAlias(new ArrayList<>());
        Domain persistedDomain = domainRepository.save(domain);
        this.mockMvc.perform(
                MockMvcRequestBuilders.get(API_LINK + "/" + persistedDomain.getId())
        ).andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("TestDomain"))
        .andExpect(jsonPath("$.resourcePath").value("/test"));
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            MongoDBInitializer.initialize(configurableApplicationContext, mongodb);
        }
    }
}
